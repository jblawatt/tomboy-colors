using System;
using Tomboy;
using Gtk;

namespace Tomboy.TomboyColors
{
	public class TomboyColorsTag : NoteTag
	{
		
		protected string _color_value;
		public const string DEFAULT_VALUE = "#FF6600";
		
		public TomboyColorsTag (string color) : base("color-" + color)
		{
			this._color_value = color;
			this.Foreground = this._color_value;
			Console.WriteLine("Tag Constructor with color: " + color);
		}
		
		public override void Initialize (string element_name)
		{
			base.Initialize (element_name);
			this.Foreground = this._color_value;	
			CanGrow = true;
			CanUndo = true;
		}
		
		public override void Write (System.Xml.XmlTextWriter xml, bool start)
		{
			if (CanSerialize) {
				if (start) {
					xml.WriteStartElement (null, ElementName, null);
					xml.WriteAttributeString("value", this._color_value);
				} else {
					xml.WriteEndElement();
				}
			}
		}
		
		public override void Read (System.Xml.XmlTextReader xml, bool start)
		{
			base.Read(xml, start);
			if (CanSerialize) {
				if (start) {
					try {
						this._color_value = xml.GetAttribute("value");
					} catch (Exception e) {
						this._color_value = DEFAULT_VALUE;	
						System.Console.WriteLine(e.Message);
					}
				}
			}
		}
	}
}

