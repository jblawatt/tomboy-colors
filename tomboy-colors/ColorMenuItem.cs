using System;
using Gtk;

namespace Tomboy.TomboyColors
{
	public class ColorMenuItem : ImageMenuItem
	{
		
		private ColorTagDefinition _colorDefinition;
		private NoteAddin _addin;
		
		public ColorMenuItem (NoteAddin addin, ColorTagDefinition def) : base(def.GetLabelText(true))
		{
			_colorDefinition = def;
			_addin = addin;
			MarkupLabel ();
			ShowAll ();
		}
		
		protected void MarkupLabel () 
		{
			Gtk.Label gtklabel = (Gtk.Label) this.Child;
			gtklabel.UseMarkup = true;
            gtklabel.UseUnderline = true;	
		}
				
		protected override void OnActivated ()
		{
			_addin.Buffer.SetActiveTag(this._colorDefinition.TagName);	
			base.OnActivated();
		}

		protected override void OnDestroyed ()
		{
			base.OnDestroyed();
		}
		
	}
}

