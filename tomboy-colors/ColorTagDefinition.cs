using System;

namespace Tomboy.TomboyColors
{
	public class ColorTagDefinition
	{
		public string TagSuffix;
		public string HTMLColor;
		public string Text;
		
		public string GetLabelText() {
			return GetLabelText(true);	
		}
		
		public string TagName {
			get {
				return String.Format("color:{0}", this.TagSuffix);	
			}
		}
	
		public string GetLabelText(bool markup) {
			if (markup) {
				// TODO: better markup to show the color
				return String.Format("<span background='{0}'>    </span> {1}", this.HTMLColor, this.Text);	
			}
			return this.Text;
		}
		
	}
	
	
}

