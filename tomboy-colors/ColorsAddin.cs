using System;
using Tomboy;
using Gtk;
using Mono.Unix;

namespace Tomboy.TomboyColors
{
	public class ColorsAddin : NoteAddin
	{
		
		object[] colors = {
			new ColorTagDefinition() {TagSuffix = "black", HTMLColor = "#000000", Text = Catalog.GetString("Black")},
			new ColorTagDefinition() {TagSuffix = "red", HTMLColor = "#FF0000", Text = Catalog.GetString("Red")},
			new ColorTagDefinition() {TagSuffix = "green", HTMLColor = "#00FF00", Text = Catalog.GetString("Green")},
			new ColorTagDefinition() {TagSuffix = "blue", HTMLColor = "#0000FF", Text = Catalog.GetString("Blue")},
			new ColorTagDefinition() {TagSuffix = "yellow", HTMLColor = "#FFFF00", Text = Catalog.GetString("Yellow")},
			new ColorTagDefinition() {TagSuffix = "orange", HTMLColor = "#FF6600", Text = Catalog.GetString("Orange")},
			new ColorTagDefinition() {TagSuffix = "cyan", HTMLColor = "#00FFFF", Text = Catalog.GetString("Cyan")},
			new ColorTagDefinition() {TagSuffix = "pink", HTMLColor = "#FF00FF", Text = Catalog.GetString("Pink")},
			new ColorTagDefinition() {TagSuffix = "gray", HTMLColor = "#C0C0C0", Text = Catalog.GetString("Gray")},
			new ColorTagDefinition() {TagSuffix = "white", HTMLColor = "#FFFFFF", Text = Catalog.GetString("White")},
		};
		
		
		public object[] Colors {
			get {
				return this.colors;	
			}
		}
		
		public override void Initialize ()
		{
			// run through all color definitions and add 
			// each of them as a new tag
			foreach (ColorTagDefinition def in this.colors) {
				
				// if that tag hasnt't already been added, add it
				if(Note.TagTable.Lookup(def.TagName) == null) {
					
					var t = new NoteTag(def.TagName) { 
						Foreground = def.HTMLColor, 
						CanGrow = true,
						CanUndo = true,
						CanSpellCheck = true
					};
					
					Note.TagTable.Add (t);
					
				}
			}
		}
		
		public override void Shutdown ()
		{
			// TODO: Implement
		}
		
		public override void OnNoteOpened ()
		{
			CreateMenuItems ();
		}
		
		protected void CreateMenuItems () 
		{
			
			// create the main menu button that all colors 
			// become grouped under
			var menu = new MenuItem(Catalog.GetString("Color"));
			
			// the submenu that will contain all the buttons
			var colorSubmenu = new Menu();
			
			// create the button to remove all applied color-tags off the selected text.
			var defaultColorItem = new DefaultColorMenuItem(this, Catalog.GetString("Default"));
			defaultColorItem.ShowNow ();
			colorSubmenu.Add(defaultColorItem);
			
			// run through all color definitions and create and add
			// menuitems for them
			foreach(ColorTagDefinition definition in this.colors) {
				var colorMenuItem = new ColorMenuItem(this, definition);
				colorMenuItem.ShowNow();
				colorSubmenu.Add(colorMenuItem);
			}		
			
			// assign submenu and show it
			menu.Submenu = colorSubmenu;
			menu.ShowAll();
			
			// add menu to the "text" menu
			this.AddTextMenuItem(menu);
			
		}
		
	}
}

