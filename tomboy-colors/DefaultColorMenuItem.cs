using System;
using Gtk;

namespace Tomboy.TomboyColors
{
	public class DefaultColorMenuItem : MenuItem
	{
		
		private ColorsAddin _addin;
		
		public DefaultColorMenuItem (ColorsAddin addin, string label) : base(label)
		{
			_addin = addin;
		}
		
		protected override void OnActivated ()
		{
			// run through all colors to check if one is activated
			// if activated deactivate it.
			foreach (ColorTagDefinition def in _addin.Colors) {
				if (_addin.Buffer.IsActiveTag(def.TagName)) {
					_addin.Buffer.ToggleActiveTag(def.TagName);	
				}
			}
			base.OnActivated ();
		}
	}
}

